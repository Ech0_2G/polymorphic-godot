class_name Player extends RigidBody2D

@export_group("Physics Variables")
@export var move_speed := 500.0
@export var rotate_speed := 45.0
@export var max_speed = 100.0

@export_group("External Scenes")
@export var radar : Node2D
@export var radar_pointer : PackedScene
@export var food_spawner : FoodSpawner

@onready var animation_player := %AnimationPlayer

var thrust := false
var rotation_input := 0.0
var direction := Vector2.ZERO

func _ready():
	animation_player.play("baby_swim")
	food_spawner.food_spawned.connect(_food_spawner_on_food_spawned)

func _food_spawner_on_food_spawned(food : Food) -> void:
	var pointer : Pointer = radar_pointer.instantiate()
	var pointer_position = (food.global_position - global_position).normalized() * 150.0
	pointer.position = pointer_position
	pointer.initialize(self, food)
	radar.add_child(pointer)

func _input(_event : InputEvent) -> void:
	thrust = Input.is_action_pressed("forward")
	rotation_input = Input.get_axis("left", "right")

	if thrust:
		animation_player.speed_scale = 1.0
	else:
		animation_player.speed_scale = 0

func _physics_process(_delta : float) -> void:
	apply_torque(rotation_input * rotate_speed)

	if thrust:
		apply_force(Vector2.RIGHT.rotated(rotation).normalized() * move_speed)

func _integrate_forces(state : PhysicsDirectBodyState2D) -> void:
	if state.linear_velocity.length() > max_speed:
		state.linear_velocity = state.linear_velocity.normalized() * max_speed
