class_name Food extends Area2D

signal food_destroyed(food : Food)

@onready var sprite : Sprite2D = %Sprite2D
@onready var timer : Timer = %DestroyTimer
@onready var particles : GPUParticles2D = %Particles

var type : FoodData.FoodType
var initialized := false
var destroyed := false
var radar_texture : Texture

func initialize(food_data : FoodData) -> void:
	type = food_data.type
	sprite.texture = food_data.texture
	particles.texture = food_data.particle_texture
	sprite.modulate = food_data.color
	particles.modulate = food_data.color
	radar_texture = food_data.radar_texture

func _on_body_entered(body : Node2D) -> void:
	if destroyed:
		return

	if is_instance_of(body, Player):
		food_destroyed.emit(self)
		sprite.queue_free()
		particles.restart()
		timer.start()
		destroyed = true

func _on_destroy_timer_completed():
	queue_free()
