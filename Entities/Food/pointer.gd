class_name Pointer extends Sprite2D

var food : Food
var player : Player

var distance_to_player := 600

func initialize(p : Player, fd : Food) -> void:
	player = p
	food = fd
	texture = food.radar_texture

func _physics_process(_delta : float) -> void:
	if player == null:
		return
	if food == null:
		return

	var direction = food.global_position - player.global_position
	var pointer_position = player.global_position + direction.normalized() * distance_to_player
	global_position = pointer_position

	if direction.length() < distance_to_player:
		visible = false

	scale = Vector2.ONE * 2000.0 / direction.length()

	look_at(food.global_position)
