class_name FoodSpawner extends Node

signal food_spawned(food : Food)

@export var food_prefab : PackedScene

@export var food_types : Array[FoodData]

func _ready() -> void:
	for _i in range(0, 20):
		spawn_food_bit()

func _on_food_spawn_timer_timeout() -> void:
	spawn_food_bit()

func spawn_food_bit() -> void:
	var food_data : FoodData = food_types[randi() % food_types.size()]
	var food_bit : Food = food_prefab.instantiate()
	var spawn_location = Vector2(
		randf_range(GameConstants.X_BOUNDS_MIN, GameConstants.X_BOUNDS_MAX),
		randf_range(GameConstants.Y_BOUNDS_MIN, GameConstants.Y_BOUNDS_MAX)
	)
	food_bit.position = spawn_location
	add_child(food_bit)
	food_bit.initialize(food_data)
	food_spawned.emit(food_bit)
