class_name FoodData extends Resource

enum FoodType {
	BABY,
	RED,
	GREEN,
	BLUE,
	YELLOW,
}

@export var type : FoodType
@export var texture : Texture
@export var radar_texture : Texture
@export var particle_texture : Texture
@export var color : Color
